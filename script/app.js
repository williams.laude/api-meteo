let apikey = '99163c97b17b7276273e9b7cb3097e42';
let wind= ['N','NE','E','SE','S','SO','O','NO']
let form = document.querySelector('form');
let icon = document.querySelector('.weather-icon');
let div = document.createElement('div');
div.className = "infos"
/**
 * function call api openweathermap with one parmeters of type string
 * @param {*} city:string
 */
let getWeather = (city) => {
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&lang=fr&appid=${apikey}`;
    fetch(url)
        .then(function (data) {
            return data.json()
        })
        .then(function (datas) {
            const icons = `https://openweathermap.org/img/wn/${datas.weather[0].icon}.png`
            console.log(datas.weather[0].icon)
            console.log(datas)
            document.querySelector('#city').innerHTML = `<i class='fas fa-map-marked-alt'></i><p>${datas.name}</p>`;
            document.querySelector('#temp').innerHTML = `<i class='fas fa-thermometer-half'></i><p>${Math.round(datas.main.temp)} °C</p>`;
            document.querySelector('#humidity').innerHTML = `<i class='fas fa-tint'></i><p>${datas.main.humidity} %</p>`;
            document.querySelector('#wind').innerHTML = `<i class='fas fa-wind'></i><p>${Math.round(datas.wind.speed * 3.6)} km/h ${wind[Math.round(datas.wind.deg/45)]}</p>`;
            document.querySelector('#tempress').innerHTML = `${datas.main.feels_like} °C`;
            document.querySelector('#tempmin').innerHTML = `${datas.main.temp_min} °C`;
            document.querySelector('#tempmax').innerHTML = `${datas.main.temp_max} °C`;
            icon.appendChild(div)
            div.innerHTML = (`<img src ="${icons}"/><p class="inline">${datas.weather[0].description}</p>`)              
        })
        .catch(function (err) {
            console.log(err)
        })
}
getWeather('Carbonne');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    let cities = document.querySelector('.inputCity').value;
    getWeather(cities);
})

